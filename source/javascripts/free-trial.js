window.addEventListener('DOMContentLoaded', () => {
  //Changes query string of SaaS CTA to that of the query string of current page
  //GLM tags passed to product for tracking
  //MVC 2: Validate that no properties in the query string are being ommitted. if they are, do nothing.
  const currentURL = new URL(window.location.href)

  if(currentURL.search){ // if there's URL parameters found on the current free trial page
    let freeTrialSaasButton = document.querySelector("[data-button='saas']")
    let freeTrialSaasLink = new URL(freeTrialSaasButton.attributes['href'].value)
    freeTrialSaasLink.search = currentURL.search
    freeTrialSaasButton.setAttribute('href', freeTrialSaasLink)
  }
});
