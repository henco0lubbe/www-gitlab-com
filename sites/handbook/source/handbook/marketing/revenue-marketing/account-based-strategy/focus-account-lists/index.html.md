---
layout: handbook-page-toc
title: "Focus Account Lists"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Focus Accounts
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list](/handbook/marketing/account-based-marketing/focus-account-list/)

### Large Focus Accounts (formerly known as the GL4300)
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#fal-enterprise](/handbook/marketing/account-based-marketing/focus-account-list/#fal-enterprise)

### Mid Market Key Accounts (formerly known as the MM4000)
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#fal-midmarket](/handbook/marketing/account-based-marketing/focus-account-list/#fal-midmarket)

### How do I identify a `focus account` or what strategy an account is in?
#### Accounts are identified in Salesforce by the GTM strategy field in Salesforce:
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#sfdc](/handbook/marketing/account-based-marketing/focus-account-list/#sfdc)

## Account hierarchy
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#new-first-order](/handbook/marketing/account-based-marketing/focus-account-list/#new-first-order)

## Connected New Customer / Net New Logo
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#connected-new](/handbook/marketing/account-based-marketing/focus-account-list/#connected-new)

### Data sources
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#data-sources](/handbook/marketing/account-based-marketing/focus-account-list/#data-sources)

### When an account moves to/from our GL4300 & MM4000 focus account lists
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#fal-development](/handbook/marketing/account-based-marketing/focus-account-list/#fal-development)

##### T-minus two weeks from end of quarter
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#fal-development](/handbook/marketing/account-based-marketing/focus-account-list/#fal-development)

##### Last day of quarter
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#fal-development](/handbook/marketing/account-based-marketing/focus-account-list/#fal-development)

##### First business day of new quarter
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#fal-development](/handbook/marketing/account-based-marketing/focus-account-list/#fal-development)

## Account Sources
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#account-sources](/handbook/marketing/account-based-marketing/focus-account-list/#account-sources)

### Aberdeen Data
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#aberdeen-data](/handbook/marketing/account-based-marketing/focus-account-list/#aberdeen-data)

## Where can I see the GL4300 and MM4000 account lists?
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#faq](/handbook/marketing/account-based-marketing/focus-account-list/#faq)

## How do I surface an account for review to be added to the `Focus Account Lists`?
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list#faq](/handbook/marketing/account-based-marketing/focus-account-list/#faq)
